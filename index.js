var arrayOfP = document.getElementsByTagName("p");
var arrayOfPHaveClassNamedFoo = [];
var aLinkToCodersTokyo = `<a href="https://school.coders.tokyo">Click me</a>`;

console.log("-------------");

for (const key in arrayOfP) {
	if (arrayOfP.hasOwnProperty(key)) {
		if (arrayOfP[`${key}`].className === "foo") {
			arrayOfPHaveClassNamedFoo.push(arrayOfP[`${key}`]);
			arrayOfP[`${key}`].innerHTML =
				arrayOfP[`${key}`].innerHTML + aLinkToCodersTokyo;
		}
	}
}

console.log(arrayOfPHaveClassNamedFoo);
